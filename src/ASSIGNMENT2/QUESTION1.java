package ASSIGNMENT2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class QUESTION1 {

	public static void main(String[] args) throws IOException {
		// taking input via command line
		Integer firstInput = Integer.parseInt(args[0]);
		Integer secondInput = Integer.parseInt(args[1]);
		System.out.println("Adding input from command line: ");
		Integer result = firstInput + secondInput;
		System.out.println(firstInput + " + " + secondInput + " = " + result);
		// taking input via Scanner class
		System.out.println("Taking input via Scanner class");
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter First number: ");
		Integer firstScannerInput = sc.nextInt();
		System.out.print("Enter Second number: ");
		Integer secondScannerInput = sc.nextInt();
		result = firstScannerInput + secondScannerInput;
		System.out.println(firstScannerInput + " + " + secondScannerInput + " = " + result);
		//comment the below line before executing the BufferedReader code
		sc.close();
		// taking input via BufferedReader class
		System.out.println("Taking input via BufferedReader class");
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter first Number: ");
		Integer firstBufferInput = Integer.parseInt(br.readLine());
		System.out.print("Enter Second Number: ");
		Integer secondBufferInput = Integer.parseInt(br.readLine());
		result = firstBufferInput + secondBufferInput;
		System.out.println(firstBufferInput + " + " + secondBufferInput + " = " + result);
	}

}
