package ASSIGNMENT2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class Cylinder {
	private Float radius;
	private Float height;

	public Float getRadius() {
		return this.radius;
	}

	public Float getHeight() {
		return this.height;
	}

	public void setRadius(Float radius) {
		this.radius = radius;
	}

	public void setHeight(Float height) {
		this.height = height;
	}

	Cylinder() {
		System.out.println("Creating Cylinder of unit dimension");
		this.setHeight(1F);
		this.setRadius(1f);
	}

	Cylinder(Float height, Float radius) {
		System.out.println("Creating Cylinder with radius : " + radius + " and height:" + height);
		this.setHeight(height);
		this.setRadius(radius);
	}

	public Double getSurfaceArea() {
		Double surf_area = 2 * Math.PI * this.getRadius() * (this.getRadius() + this.getHeight());
		return surf_area;
	}

	public Double getVolume() {
		Double volume = 2 * Math.PI * Math.pow(this.getRadius(), 2) * this.getHeight();
		return volume;
	}
}

public class QUESTION2 {

	public static void main(String[] args) throws IOException {
		BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Do you want to Enter dimensions of the  cylinder:  Press Y for yes and N for no\t");
		Character response = input.readLine().charAt(0);
		if (response == 'Y' || response == 'y') {
			System.out.print("Enter the height of the cylinder: ");
			Float height = Float.valueOf(input.readLine());
			System.out.print("Enter the radius of the cylinder: ");
			Float radius = Float.valueOf(input.readLine());
			Cylinder cylinder = new Cylinder(height, radius);
			System.out.println("The surface area of the cylinder: " + cylinder.getSurfaceArea());
			System.out.println("The volume of the cylinder: " + cylinder.getVolume());
		} else {
			Cylinder cylinder = new Cylinder();
			System.out.println("The surface area of the cylinder: " + cylinder.getSurfaceArea());
			System.out.println("The volume of the cylinder: " + cylinder.getVolume());
		}
		input.close();
	}

}
